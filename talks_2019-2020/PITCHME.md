![Logo](assets/background.png)

---

## Nos Objectifs 

Facilitier le partage entre les développeurs auvergnats autour des technologies Microsoft (Mais pas que !)

---

## Le format

* 1 évènement par mois
* 1 heure de présentation technique
* 1 pot offerts par nos sponsors aux participants

---

![Logo](assets/sponsors.png)

---

## Les prochaines dates  

* Devoxx4Kids - 12 Octobre 
* MUG - 14 Octobre - Code week
* JUG - 17 Octobre 
* Clermontech API Hour 23 octobre  
* MUG - 12 Novembre - PowerBI c'est fun
* MUG - 5 Décembre - Chatbot sur la plate forme MS



