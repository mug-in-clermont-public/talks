![Logo](assets/background.png)

---

## Qui sommes nous ? 

* Damien Fourniat
* Jérôme Bolot 
* Pierre Plagnes
* Sylvain Le-Gouellec
* Kevin Beaugrand

---

## Nos sponsors 

* [CGI](htp://www.cgi.com) : ESN Clermontoise, Plus de 500 personnes en Auvergne

---

## Nos Objectifs 

Facilitier le partage entre les développeurs auvergnats autour des technologies Microsoft (Mais pas que !)

---

### Le format

* 1 évènement tous les 2 mois
* 1 heure de présentation technique
* 1 pot offert aux participants

--- 

### Not only Microsoft 

Nous parlons aussi de :

* Linux
* Docker
* K8S
* ... 

---

### Les dernières sessions 

* M1 - Xamarin (Juin 2017) : 87 pers.
* M2 - .Net Core (Octobre 2017) : 90 pers.
* M3 - Michelin AI'nnov (décembre 2017) 
* M4 - Le dev dans le monde Ms (Mars 2018) : 76 p
* M5 - TFS Wonderland (Mai 2018) : 54 pers.
* M6 - Un histoire de container : (Michelin, IUT)
* M7 - Le WPF dans tous ses états : 85 pers.

---

### Autres sessions
Clermont ClubInfo (Juin 2018)
* Présenté par les étudiants du ClubInfo de Marc Chevaldonné
* Projet de developpement Xamarin
* Application de programmation d'automates pour les enfants

--- 

### Les participants 

* Des employés d'ESN (CGI, Accenture, Atos, Avanade, Sopra, Modis)
* Des startups
* Des étudiants (IUT, U'DEV, ISIMA, License)
* Des employés d'entreprises non-informatique (Michelin, Limagrain, Auber&Duval, Addup)

---

### Liens utiles 

* [Meetup](http://github.com)https://www.meetup.com/fr-FR/MugInClermont/)
* [Gitlab](https://gitlab.com/mug-in-clermont-public)
* [Twitter](https://twitter.com/muginclermont)
* [Slack](https://muginclermont.slack.com/messages)

---

## Merci

