# MUG 09 
### Journal d'un dev Java dans un monde .Net
---?image=assets/sponsors.png
# Merci !
![alt text](https://gitlab.com/mug-in-clermont-public/talks/raw/master/S09/assets/sponsors.png "")

---
## Evénements 

**LavaJug**
- 7 conseils pour démarrer avec Spark **(28 Février)**

**Clermont'ech**
- LavaJug APIHour #41 **(7 Mars)**
- Clermontech.php Super Apéro 2019 **(14 Mars)**

*https://planet.clermontech.org/*

---
## Prochains events avec le Mug
- TypeScript - Sylvain Pontoreau - Bivouac - 21 mars - 
Coorganisé par les JUG, Clermontech et Bivouac 

- MUG 11 - Xamarin vers des progressives Web App - IUT - 9 avril IUT



